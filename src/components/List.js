import React from 'react';

export default function List (props) {
    return(
        <div>
            <ul className="ulList">
                {/* {props.theList.forEach((element,index )=> <li>{element[index]}</li>)} */}
                {props.theList.map((material,index) => <li><span className="list">{("0" + Math.floor((material / 60000) % 60)).slice(-2)}:</span>
                <span className="list">{("0" + Math.floor((material / 1000) % 60)).slice(-2)}:</span>
                <span className="list">{("0" + ((material / 10) % 100)).slice(-2)}</span></li>)}
            </ul>
            
        </div>
    )
}