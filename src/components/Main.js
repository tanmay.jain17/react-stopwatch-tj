import React, { useState, useEffect } from 'react';
import Start from './Start'
import Stop from './Stop'
import Reset from './Reset'
import Lap from './Lap'
import List from './List'


export default function Main() {

    const [time, setTime] = useState(0);
    const [interv, setInterv] = useState();
    const [lapList, setLapList] = useState([]);
    const [status, setStatus] = useState(false);

    function startTheTime() {
        setStatus(true)
        setInterv(setInterval(() => {
            setTime((prevTime) => prevTime + 10)
        }, 10))
    }
    function stopTheTime() {
        setStatus(false)
        clearInterval(interv)
    }
    function resetTheTime() {
        setTime(0)
        setLapList([])
    }
    function lapTime() {
        const newList = [...lapList, time]
        setLapList(newList)
    }

    return (
        <div className="App">
            <div className="txtDiv">
                <h1 className="txt">{("0" + Math.floor((time / 60000) % 60)).slice(-2)}:</h1>
                <h1 className="txt">{("0" + Math.floor((time / 1000) % 60)).slice(-2)}:</h1>
                <h1 className="txt">{("0" + ((time / 10) % 100)).slice(-2)}</h1>
            </div>
            <div className="butnDiv">
                <span className="butns">
                    {!status && <Start startIt={() => startTheTime()} />}
                </span>
                <span className="butnl">
                    {status && <Stop stopIt={() => stopTheTime()} />}
                </span>
                <span className="butns butnd">
                    {!status && (time !== 0) && <Reset resetIt={() => resetTheTime()} />}
                </span>
                <span className="butnl butnd">
                    {status && <Lap lapIt={() => lapTime()} />}
                </span>
            </div>
                
            
            <div className="listDiv">
            <List  theList={lapList} />
            </div>
            
        </div>
    );
}