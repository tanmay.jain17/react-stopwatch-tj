import React,{useState,useEffect} from 'react';

export default function Reset(props){
    return(
        <div>
            <button type="button" class="btn btn-light btn-large" onClick={props.resetIt}>RESET</button>
        </div>
    );
}