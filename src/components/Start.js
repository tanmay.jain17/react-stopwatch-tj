import React,{useState,useEffect} from 'react';

export default function Start(props){
    return(
        <div>
            <button type="button" class="btn btn-light btn-large" onClick={props.startIt}>START</button>
        </div>
    );
}